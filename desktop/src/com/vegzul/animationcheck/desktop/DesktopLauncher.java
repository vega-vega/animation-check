package com.vegzul.animationcheck.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vegzul.animationcheck.Main;
import com.vegzul.animationcheck.screen.MainScreen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = (int) (800 + MainScreen.MENU_WIDTH);
		config.height = 600;
		config.fullscreen = false;
		config.resizable = false;

		new LwjglApplication(new Main(), config);
	}
}

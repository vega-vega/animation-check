package com.vegzul.animationcheck.model;

import com.vegzul.imagepackerjson.model.AnimationLayerModel;

public class AnimationData {

    private AnimationLayerModel animationLayerModel;
    private String path;
    private int countFrame;
    private int tileWidth;
    private int tileHeight;

    public AnimationData(AnimationLayerModel animationLayerModel, String path, int countFrame, int tileWidth, int tileHeight) {
        this.animationLayerModel = animationLayerModel;
        this.path = path;
        this.countFrame = countFrame;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    public AnimationLayerModel getAnimationLayerModel() {
        return animationLayerModel;
    }

    public void setAnimationLayerModel(AnimationLayerModel animationLayerModel) {
        this.animationLayerModel = animationLayerModel;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCountFrame() {
        return countFrame;
    }

    public void setCountFrame(int countFrame) {
        this.countFrame = countFrame;
    }

    public int getTileWidth() {
        return tileWidth;
    }

    public void setTileWidth(int tileWidth) {
        this.tileWidth = tileWidth;
    }

    public int getTileHeight() {
        return tileHeight;
    }

    public void setTileHeight(int tileHeight) {
        this.tileHeight = tileHeight;
    }
}

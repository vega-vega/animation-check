package com.vegzul.animationcheck;

import com.badlogic.gdx.Game;
import com.kotcrab.vis.ui.VisUI;
import com.vegzul.animationcheck.resource.Resource;
import com.vegzul.animationcheck.screen.MainScreen;

public class Main extends Game {

	public static Resource resource;
	public static boolean IS_FULL_SCREEN = false;

	@Override
	public void create () {
		VisUI.load();
	    resource = new Resource();
		setScreen(new MainScreen());
	}
	
	@Override
	public void dispose () {
		resource.dispose();
		VisUI.dispose();
	}
}

package com.vegzul.animationcheck.enumData;

import com.vegzul.animationcheck.screen.MainScreen;

public enum WindowSize {
    D800x600("800x600", 800, 600),
    D1024x768("1024x768", 1024, 768),
    D1152x864("1152x864", 1152, 864),
    D1280x800("1280x800", 1280, 800),
    D1280x1024("1280x1024", 1280, 1024),
    D1600x900("1600x900", 1600, 900),
    D1680x1050("1680x1050", 1680, 1050),
    D1920x1080("1920x1080", 1920, 1080),
    D1920x1200("1920x1200", 1920, 1200);

    private String name;
    private int width;
    private int height;

    WindowSize(String name, int width, int height) {
        this.name = name;
        this.width = (int) (width + MainScreen.MENU_WIDTH);
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return name;
    }
}

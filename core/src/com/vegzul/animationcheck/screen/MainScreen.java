package com.vegzul.animationcheck.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.kotcrab.vis.ui.widget.file.FileTypeFilter;
import com.vegzul.animationcheck.group.MenuGroup;
import com.vegzul.animationcheck.group.ShowGroup;

/**
 * Главный экран приложения.
 */
public class MainScreen implements Screen {

    public static final float MENU_WIDTH = 250;

    private Viewport viewport;
    private static Stage stage;
    private ShowGroup showGroup;
    private MenuGroup menuGroup;
    public static boolean isBlack = true;

    private static final FileChooser fileChooser = new FileChooser(FileChooser.Mode.OPEN);
    private static final FileChooser saveFileChooser = new FileChooser(FileChooser.Mode.SAVE);

    @Override
    public void show() {
        viewport = new ScreenViewport(new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);

        FileTypeFilter typeFilter = new FileTypeFilter(false);
        typeFilter.addRule("Image files (png, jpg)", "png", "jpg");

        FileChooser.setSaveLastDirectory(true);
        fileChooser.setSelectionMode(FileChooser.SelectionMode.FILES_AND_DIRECTORIES);
        fileChooser.setFileTypeFilter(typeFilter);

        FileTypeFilter saveTypeFilter = new FileTypeFilter(false);
        saveTypeFilter.addRule("json", "json");
        saveFileChooser.setSelectionMode(FileChooser.SelectionMode.FILES_AND_DIRECTORIES);
        saveFileChooser.setFileTypeFilter(saveTypeFilter);

        showGroup = new ShowGroup(0, 0, (Gdx.graphics.getWidth() - MENU_WIDTH), Gdx.graphics.getHeight());
        menuGroup = new MenuGroup((Gdx.graphics.getWidth() - MENU_WIDTH), 0, MENU_WIDTH, Gdx.graphics.getHeight(), showGroup);

        stage.addActor(showGroup);
        stage.addActor(menuGroup);
    }

    @Override
    public void render(float delta) {
        if (isBlack) {
            Gdx.gl.glClearColor(0, 0, 0, 1);
        } else {
            Gdx.gl.glClearColor(1, 1, 1, 1);
        }
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    public static void callFileChooser(FileChooserAdapter fileChooserAdapter) {
        fileChooser.setListener(fileChooserAdapter);
        stage.addActor(fileChooser);
    }

    public static void callSaveFileChooser(String s) {
        saveFileChooser.setListener(new FileChooserAdapter() {
            @Override
            public void selected(Array<FileHandle> files) {
                files.first().writeString(s, false);
            }
        });
        stage.addActor(saveFileChooser);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        showGroup.setBounds(0, 0, (width - MENU_WIDTH), height);
        menuGroup.setBounds((width - MENU_WIDTH), 0, MENU_WIDTH, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

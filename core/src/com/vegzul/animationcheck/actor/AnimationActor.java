package com.vegzul.animationcheck.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationActor extends CommonActor {

    private Animation<TextureRegion> regionAnimation;
    private float stateTime = 0;

    public AnimationActor(Animation<TextureRegion> regionAnimation, float x, float y, float width, float height) {
        this.regionAnimation = regionAnimation;
        setBounds(x, y, width, height);
    }

    public Animation<TextureRegion> getRegionAnimation() {
        return regionAnimation;
    }

    public void setRegionAnimation(Animation<TextureRegion> regionAnimation) {
        this.regionAnimation = regionAnimation;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();

        sprite.setRegion(regionAnimation.getKeyFrame(stateTime, true));
        super.draw(batch, parentAlpha);
    }
}

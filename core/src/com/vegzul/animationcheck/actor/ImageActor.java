package com.vegzul.animationcheck.actor;

import com.badlogic.gdx.graphics.Texture;

public class ImageActor extends CommonActor {

    public ImageActor(Texture texture) {
        super(texture);
    }

    public ImageActor(Texture texture, float x, float y, float width, float height) {
        super(texture, x, y, width, height);
    }

    @Override
    public Texture getTexture() {
        return super.getTexture();
    }
}

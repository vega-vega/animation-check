package com.vegzul.animationcheck.actor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Общий класс для актеров.
 */
public abstract class CommonActor extends Actor {

    protected Sprite sprite;

    public CommonActor() {
        sprite = new Sprite();
    }

    protected CommonActor(Texture texture) {
        sprite = new Sprite(texture);
    }

    protected CommonActor(Texture texture, float x, float y, float width, float height) {
        this(texture);

        setBounds(x, y, width, height);
    }

    protected Texture getTexture() {
        return sprite.getTexture();
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        sprite.draw(batch);
    }

    @Override
    public void act(float delta) {
        sprite.setBounds(getX(), getY(), getWidth(), getHeight());
    }
}

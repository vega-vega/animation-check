package com.vegzul.animationcheck.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Disposable;

public class Resource implements Disposable {

    public Skin uiSkin;

    {
        uiSkin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    }

    @Override
    public void dispose() {
        uiSkin.dispose();
    }
}

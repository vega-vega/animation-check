package com.vegzul.animationcheck.group;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.vegzul.animationcheck.actor.AnimationActor;
import com.vegzul.animationcheck.actor.CommonActor;
import com.vegzul.animationcheck.actor.ImageActor;
import com.vegzul.imagepackerjson.model.AnimationLayerModel;
import com.vegzul.imagepackerjson.model.ImageLayerModel;

import java.util.Arrays;

public class ShowGroup extends Group {

    private static final float MARGIN_WIDTH = 5;
    private static final float MARGIN_HEIGHT = 5;

    public ShowGroup (float x, float y, float width, float height) {
        setBounds(x, y, width, height);
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        updateActorSize();
    }

    public void addImage(ImageLayerModel imageLayerModel) {
        Texture texture = imageLayerModel.getRegion().getTexture();
        float width = imageLayerModel.getWidth() == null ? texture.getWidth() : imageLayerModel.getWidth();
        float height = imageLayerModel.getHeight() == null ? texture.getHeight() : imageLayerModel.getHeight();


        if (imageLayerModel.getWidth() != null || imageLayerModel.getHeight() != null) {
            texture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMapLinearLinear);
        }

        if (imageLayerModel.getX() != null || imageLayerModel.getY() != null) {
            float x = imageLayerModel.getX() == null ? 0 : imageLayerModel.getX();
            float y = imageLayerModel.getY() == null ? 0 : imageLayerModel.getY();

            addActor(width, height, x, y, texture, null);
        } else {
            addActor(width, height, texture, null);
        }
    }

    public void addAnimation(AnimationLayerModel animationLayerModel) {
        Animation<TextureRegion> regionAnimation = animationLayerModel.getAnimation();
        float width = animationLayerModel.getWidth() == null ? regionAnimation.getKeyFrames()[0].getRegionWidth() : animationLayerModel.getWidth();
        float height = animationLayerModel.getHeight() == null ? regionAnimation.getKeyFrames()[0].getRegionHeight() : animationLayerModel.getHeight();

        if (animationLayerModel.getWidth() != null || animationLayerModel.getHeight() != null) {
            Arrays.asList(regionAnimation.getKeyFrames()).forEach(textureRegion -> textureRegion.getTexture()
                    .setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMapLinearLinear));
        }

        if (animationLayerModel.getX() != null || animationLayerModel.getY() != null) {
            float y = animationLayerModel.getY() == null ? 0 : animationLayerModel.getY();
            float x = animationLayerModel.getX() == null ? 0 : animationLayerModel.getX();

            addActor(width, height, x, y, null, regionAnimation);
        } else {
            addActor(width, height, null, regionAnimation);
        }
    }

    private void updateActorSize() {
        for (Actor actor : getChildren()) {
            float width = getWidth() - (MARGIN_WIDTH * 2);
            float height = getHeight() - (MARGIN_HEIGHT * 2);

            float frameWidth;
            float frameHeight;
            if (actor instanceof ImageActor) {
                frameWidth = ((ImageActor) actor).getTexture().getWidth();
                frameHeight = ((ImageActor) actor).getTexture().getHeight();
            } else {
                frameWidth = ((AnimationActor) actor).getRegionAnimation().getKeyFrames()[0].getRegionWidth();
                frameHeight = ((AnimationActor) actor).getRegionAnimation().getKeyFrames()[0].getRegionHeight();
            }

            float marginWidth = width - frameWidth;
            float marginHeight = height - frameHeight;

            if (marginWidth < 0 || marginHeight < 0) {
                float different = frameWidth / frameHeight;
                float actorWidth, actorHeight;

                boolean isManiWidth = (marginWidth < marginHeight);

                if (isManiWidth) {
                    actorWidth = frameWidth + marginWidth;
                    marginWidth = MARGIN_WIDTH;
                    actorHeight = frameHeight + (marginWidth / different);
                    marginHeight = height - actorHeight;
                } else {
                    actorWidth = frameWidth + (marginHeight / different);
                    marginWidth = width - actorWidth;
                    actorHeight = frameHeight + marginHeight;
                    marginHeight = MARGIN_HEIGHT;
                }

                actor.setBounds(marginWidth / 2, marginHeight / 2, actorWidth, actorHeight);
            } else {
                actor.setBounds(marginWidth / 2, marginHeight / 2, frameWidth, frameHeight);
            }
        }
    }

    private void addActor(float spriteWidth, float spriteHeight, Texture texture, Animation<TextureRegion> regionAnimation) {
        float groupWidth = getWidth() - (MARGIN_WIDTH * 2);
        float groupHeight = getHeight() - (MARGIN_HEIGHT * 2);

        float marginWidth = groupWidth - spriteWidth;
        float marginHeight = groupHeight - spriteHeight;

        CommonActor actor;
        if (marginWidth < 0 || marginHeight < 0) {
            float different = spriteWidth / spriteHeight;
            float actorWidth, actorHeight;

            boolean isManiWidth = (marginWidth < marginHeight);

            if (isManiWidth) {
                actorWidth = spriteWidth + marginWidth;
                actorHeight = spriteHeight + (marginWidth / different);
                marginWidth = MARGIN_WIDTH;
                marginHeight = groupHeight - actorHeight;
            } else {
                actorWidth = spriteWidth + (marginHeight / different);
                actorHeight = spriteHeight + marginHeight;
                marginWidth = groupWidth - actorWidth;
                marginHeight = MARGIN_HEIGHT;
            }

            if (regionAnimation == null) {
                actor = new ImageActor(texture, marginWidth / 2, marginHeight / 2, actorWidth, actorHeight);
            } else {
                actor = new AnimationActor(regionAnimation, marginWidth / 2, marginHeight / 2, actorWidth, actorHeight);
            }
        } else {
            if (regionAnimation == null) {
                actor = new ImageActor(texture, marginWidth / 2, marginHeight / 2, spriteWidth, spriteHeight);
            } else {
                actor = new AnimationActor(regionAnimation, marginWidth / 2, marginHeight / 2, spriteWidth, spriteHeight);
            }
        }

        addActor(actor);
    }

    private void addActor(float spriteWidth, float spriteHeight, float x, float y, Texture texture, Animation<TextureRegion> regionAnimation) {
        float groupWidth = getWidth() - (MARGIN_WIDTH * 2);
        float groupHeight = getHeight() - (MARGIN_HEIGHT * 2);

        x = groupWidth < x ? groupWidth : x;
        y = groupHeight < y ? groupHeight : y;

        CommonActor actor;
        if (regionAnimation == null) {
            actor = new ImageActor(texture, x, y, spriteWidth, spriteHeight);
        } else {
            actor = new AnimationActor(regionAnimation, x, y, spriteWidth, spriteHeight);
        }

        addActor(actor);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}

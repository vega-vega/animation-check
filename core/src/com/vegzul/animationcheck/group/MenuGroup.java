package com.vegzul.animationcheck.group;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.vegzul.animationcheck.Main;
import com.vegzul.animationcheck.enumData.WindowSize;
import com.vegzul.animationcheck.model.AnimationData;
import com.vegzul.imagepackerjson.model.AnimationLayerModel;
import com.vegzul.imagepackerjson.model.AnimationModel;
import com.vegzul.imagepackerjson.model.ImageLayerModel;
import com.vegzul.imagepackerjson.model.ImageModel;
import com.vegzul.animationcheck.screen.MainScreen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class MenuGroup extends Group {

    private static final float PADDING = 5;
    private static final float SPACE = 5;
    private static final float SPACE_SCROLL_ELEMENT = 8;
    private static final String CHANGE_TO_ANIMATION = "Change to animation";
    private static final String CHANGE_TO_IMAGE = "Change to image";
    private static final String ATLAS_ANIMATION = "Atlas";
    private static final String IMAGE_ANIMATION = "Image";
    private static final String BACKGROUND_BLACK = "Black";
    private static final String BACKGROUND_WHITE = "White";

    private final ShowGroup showGroup;

    private Table mainWindow;
    private TextButton changeButton, clearButton, saveButton, addLayerButton, backgroundColorButton;
    private SelectBox windowSizeSelect;
    private Table scrollImageTable = new Table();
    private Table scrollAnimationTable = new Table();
    private ScrollPane scrollImagePane, scrollAnimationPane;
    private Json json;

    private boolean isImage = false;

    private final Array<ImageLayerModel> imageLayerArray = new Array<>();
    private final Array<AnimationData> animationDataArray = new Array<>();

    public MenuGroup(float x, float y, float width, float height, ShowGroup showGroup) {
        setBounds(x, y, width, height);
        createWindow();
        addActor(mainWindow);
        json = new Json();
        this.showGroup = showGroup;
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        updateMenuSize();
    }

    private void updateMenuSize() {
        if (mainWindow == null) {
            return;
        }

        mainWindow.setHeight(getHeight() - (PADDING * 2));

        if (isImage) {
            drawImageLayer();
        } else {
            drawAnimationLayer();
        }
    }

    private void createWindow() {
        mainWindow = new Table(Main.resource.uiSkin);
        mainWindow.setBounds(PADDING, PADDING, (getWidth() - (PADDING * 2)), (getHeight() - (PADDING * 2)));
        mainWindow.top();
        mainWindow.defaults().pad(SPACE).expandX();

        changeButton = new TextButton(isImage ? CHANGE_TO_ANIMATION : CHANGE_TO_IMAGE, Main.resource.uiSkin);
        mainWindow.add(changeButton).width(mainWindow.getWidth() - (SPACE * 2)).colspan(2);
        mainWindow.row();

        createClearButton();
        createSaveButton();
        mainWindow.add(clearButton).width((mainWindow.getWidth() - (SPACE * 4))  / 2);
        mainWindow.add(saveButton).width((mainWindow.getWidth() - (SPACE * 4))  / 2);
        mainWindow.row();

        createWindowSizeSelect();
        createBackgroundColorButton();
        mainWindow.add(windowSizeSelect).width((mainWindow.getWidth() - (SPACE * 4))  / 2);
        mainWindow.add(backgroundColorButton).width((mainWindow.getWidth() - (SPACE * 4))  / 2);
        mainWindow.row();

        createAddLayerButton();
        mainWindow.add(addLayerButton).width(mainWindow.getWidth() - (SPACE * 2)).colspan(2);
        mainWindow.row();

        scrollImagePane = new ScrollPane(scrollImageTable, Main.resource.uiSkin);
        scrollAnimationPane = new ScrollPane(scrollAnimationTable, Main.resource.uiSkin);

        final Cell scrollCell = mainWindow.add().colspan(2);
        scrollCell.setActor(isImage ? scrollImagePane : scrollAnimationPane);

        changeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                isImage = !isImage;

                if (isImage) {
                    changeButton.setText(CHANGE_TO_ANIMATION);
                    scrollCell.setActor(scrollImagePane);
                    drawImageLayer();
                } else {
                    changeButton.setText(CHANGE_TO_IMAGE);
                    scrollCell.setActor(scrollAnimationPane);
                    drawAnimationLayer();
                }
            }
        });

        addImageScrollElement();
        addAnimationScrollElement();
    }

    private void createSaveButton() {
        saveButton = new TextButton("Save", Main.resource.uiSkin);
        saveButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (isImage) {
                    Array<ImageLayerModel> layerArray = new Array<>();
                    for (ImageLayerModel layerModel : imageLayerArray) {
                        if (layerModel.getRegion() != null) {
                            layerArray.add(layerModel);
                        }
                    }

                    ImageModel imageModel = new ImageModel();
                    imageModel.setImageLayerArray(layerArray);

                    MainScreen.callSaveFileChooser(json.toJson(imageModel));
                } else {
                    Array<AnimationLayerModel> layerArray = new Array<>();
                    for (AnimationData animationData : animationDataArray) {
                        AnimationLayerModel layerModel = animationData.getAnimationLayerModel();
                        if (layerModel.getAnimationFrame() != null) {
                           layerArray.add(layerModel);
                        }
                    }

                    AnimationModel animationModel = new AnimationModel();
                    animationModel.setAnimationLayerModelArray(layerArray);

                    MainScreen.callSaveFileChooser(json.toJson(animationModel));
                }
            }
        });
    }

    private void createClearButton() {
        clearButton = new TextButton("Clear", Main.resource.uiSkin);
        clearButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (isImage) {
                    scrollImageTable.clearChildren();
                } else {
                    scrollAnimationTable.clearChildren();
                }
            }
        });
    }

    private void createAddLayerButton() {
        addLayerButton = new TextButton("Add layer", Main.resource.uiSkin);
        addLayerButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (isImage) {
                    addImageScrollElement();
                } else {
                    addAnimationScrollElement();
                }
            }
        });
    }

    private void createWindowSizeSelect() {
        windowSizeSelect = new SelectBox(Main.resource.uiSkin);
        windowSizeSelect.setItems(WindowSize.values());
        windowSizeSelect.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                WindowSize size = (WindowSize) windowSizeSelect.getSelected();

                if (Main.IS_FULL_SCREEN) {
                    Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                } else {
                    Gdx.graphics.setWindowedMode(size.getWidth(), size.getHeight());
                }
            }
        });
    }

    private void createBackgroundColorButton() {
        backgroundColorButton = new TextButton(MainScreen.isBlack ? BACKGROUND_WHITE : BACKGROUND_BLACK, Main.resource.uiSkin);
        backgroundColorButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MainScreen.isBlack = !MainScreen.isBlack;
                backgroundColorButton.setText(MainScreen.isBlack ? BACKGROUND_WHITE : BACKGROUND_BLACK);
            }
        });
    }

    private void drawImageLayer() {
        showGroup.clearChildren();

        for (ImageLayerModel imageLayerModel : imageLayerArray) {
            if (imageLayerModel != null && imageLayerModel.getRegion() != null) {
                showGroup.addImage(imageLayerModel);
            }
        }
    }

    private void drawAnimationLayer() {
        showGroup.clearChildren();

        for (AnimationData animationData : animationDataArray) {
            AnimationLayerModel animationLayerModel = animationData.getAnimationLayerModel();
            if (animationLayerModel != null && animationLayerModel.getAnimationFrame() != null) {
                showGroup.addAnimation(animationLayerModel);
            }
        }
    }

    private void addImageScrollElement() {
        final TextField imagePath = new TextField("", Main.resource.uiSkin);
        final Table groupTable = new Table();
        groupTable.setBackground(Main.resource.uiSkin.getDrawable("border"));
        final TextButton pathButton = new TextButton("...", Main.resource.uiSkin);
        final TextButton deleteRow = new TextButton("X", Main.resource.uiSkin);
        final TextField widthField = new TextField("", Main.resource.uiSkin);
        final TextField heightField = new TextField("", Main.resource.uiSkin);
        final TextField xField = new TextField("", Main.resource.uiSkin);
        final TextField yField = new TextField("", Main.resource.uiSkin);
        final TextButton addGroupButton = new TextButton("Add group", Main.resource.uiSkin);
        ImageLayerModel imageLayerModel = new ImageLayerModel();
        List<Integer> groupList = new ArrayList<>();
        imageLayerModel.setGroup(groupList);
        Map<Integer, Integer> groupValueMap = new HashMap<>();
        imageLayerArray.add(imageLayerModel);

        Table table = addImageRow(imagePath, addGroupButton, groupTable, pathButton, deleteRow, widthField, heightField, xField, yField);

        imagePath.setTextFieldListener((TextField textField, char c) -> {
            try {
                TextureRegion region = new TextureRegion(new Texture(Gdx.files.internal(textField.getText()), true));
                imageLayerModel.setRegion(region);
            } catch (Exception ignored) {
                imageLayerModel.setRegion(null);
            }
            drawImageLayer();
        });

        widthField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setImageLayer(imageLayerModel, getFloatFromField(widthField), getFloatFromField(heightField), getFloatFromField(xField), getFloatFromField(yField));
            }
        });
        heightField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setImageLayer(imageLayerModel, getFloatFromField(widthField), getFloatFromField(heightField), getFloatFromField(xField), getFloatFromField(yField));
            }
        });
        xField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setImageLayer(imageLayerModel, getFloatFromField(widthField), getFloatFromField(heightField), getFloatFromField(xField), getFloatFromField(yField));
            }
        });
        yField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setImageLayer(imageLayerModel, getFloatFromField(widthField), getFloatFromField(heightField), getFloatFromField(xField), getFloatFromField(yField));
            }
        });

        pathButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MainScreen.callFileChooser(new FileChooserAdapter() {
                    @Override
                    public void selected(Array<FileHandle> files) {
                        imagePath.setText(files.first().path());
                        try {
                            TextureRegion region = new TextureRegion(new Texture(Gdx.files.internal(files.first().path()), true));
                            imageLayerModel.setRegion(region);
                        } catch (Exception ignored) {
                            imageLayerModel.setRegion(null);
                        }
                        drawImageLayer();
                    }
                });
            }
        });

        deleteRow.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                table.clear();
                imageLayerArray.removeValue(imageLayerModel, true);

                Array<Cell> cellArray = scrollImageTable.getCells();
                for (Cell cell : cellArray) {
                    if (cell.getActor() == table) {
                        cell.clearActor().pad(0);
                    }
                }

                cellArray.sort((Cell c1, Cell c2) -> {
                    if (c1.getActor() != null && c2.getActor() == null) {
                        return -1;
                    }

                    if (c1.getActor() == null && c2.getActor() != null) {
                        return 1;
                    }

                    return 0;
                });

                drawImageLayer();
            }
        });

        addGroupButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                addGroupCell(groupTable, groupValueMap, groupList);
            }
        });
    }

    private Table addImageRow(
            final TextField imagePath,
            final TextButton addGroupButton,
            final Table groupTable,
            final TextButton pathButton,
            final TextButton deleteButton,
            final TextField widthField,
            final TextField heightField,
            final TextField xField,
            final TextField yField
    ) {
        Table table = null;
        Array<Cell> cellArray = scrollImageTable.getCells();
        for (Cell cell : cellArray) {
            if (cell.getActor() == null) {
                table = new Table();
                cell.setActor(table).pad(1);
                break;
            }
        }

        if (table == null) {
            table = new Table();
            scrollImageTable.add(table).pad(1);
        }
        table.setBackground(Main.resource.uiSkin.getDrawable("border"));

        float rowWidth = (mainWindow.getWidth() / 10) * 0.92f;

        table.add(imagePath)
                .colspan(2)
                .width((rowWidth * 6) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(pathButton)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padRight(SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(deleteButton)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padRight(SPACE_SCROLL_ELEMENT * 2)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.row();

        table.add(widthField)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(heightField)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(xField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2)
                .padRight(SPACE_SCROLL_ELEMENT);
        table.add(yField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2)
                .padRight(SPACE_SCROLL_ELEMENT * 2);
        table.row();

        table.add(addGroupButton)
                .colspan(2)
                .width((rowWidth * 6) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(groupTable).width((rowWidth * 4) - (SPACE_SCROLL_ELEMENT * 2))
                .colspan(2)
                .padRight(SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);

        scrollImageTable.row();
        return table;
    }

    private void addGroupCell(final Table groupTable, final Map<Integer, Integer> groupValueMap, List<Integer> groupList) {
        AtomicInteger lastIndex = new AtomicInteger(0);
        groupValueMap.keySet().forEach(integer -> lastIndex.set(lastIndex.get() < integer ? integer : lastIndex.get()));
        final Integer currentIndex = lastIndex.incrementAndGet();
        groupValueMap.put(currentIndex, null);
        final TextField groupNumber = new TextField("", Main.resource.uiSkin);
        final TextButton deleteButton = new TextButton("X", Main.resource.uiSkin);

        Cell groupNumberCell = null;
        Cell deleteButtonCell = null;
        Array<Cell> cellArray = groupTable.getCells();
        for (int i = 0; i < cellArray.size; i++) {
            if (cellArray.get(i).getActor() == null) {
                groupNumberCell = cellArray.get(i).setActor(groupNumber);
                deleteButtonCell = cellArray.get(i + 1).setActor(deleteButton);
                break;
            }
        }
        if (groupNumberCell == null || deleteButtonCell == null) {
            groupNumberCell = groupTable.add(groupNumber);
            deleteButtonCell = groupTable.add(deleteButton);
        }

        float rowWidth = (mainWindow.getWidth() / 10) * 0.80f;
        groupNumberCell.width((rowWidth * 2) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        deleteButtonCell.width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padRight(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);

        groupTable.row();

        groupNumber.setTextFieldListener((textField, c) -> {
            Integer newValue = null;
            try {
                newValue = new Integer(textField.getText());
            } catch (NumberFormatException e) {
                textField.setText("");
            }

            groupValueMap.put(currentIndex, newValue);
            groupList.clear();
            groupValueMap.forEach((index, value) -> groupList.add(value));
        });

        deleteButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Array<Cell> cellArray = groupTable.getCells();
                for (int i = 0; i < cellArray.size; i++) {
                    if (cellArray.get(i).getActor() == groupNumber) {
                        cellArray.get(i).clearActor().pad(0).space(0);
                        cellArray.get(i + 1).clearActor().pad(0).space(0);

                        cellArray.sort((Cell o1, Cell o2) -> {
                            if (o1.getActor() == null && o2.getActor() != null) {
                                return 1;
                            }

                            if (o2.getActor() == null && o1.getActor() != null) {
                                return -1;
                            }

                            return 0;
                        });

                        groupValueMap.remove(currentIndex);

                        groupList.clear();
                        groupValueMap.forEach((index, value) -> groupList.add(value));

                        if (isImage) {
                            drawImageLayer();
                        } else {
                            drawAnimationLayer();
                        }
                        break;
                    }
                }
            }
        });
    }

    private void addAnimationScrollElement() {
        final Table groupTable = new Table();
        groupTable.setBackground(Main.resource.uiSkin.getDrawable("border"));
        final AtomicBoolean isAtlas = new AtomicBoolean(true);
        final TextField inputField = new TextField("", Main.resource.uiSkin);
        final TextButton pathButton = new TextButton("...", Main.resource.uiSkin);
        final TextButton deleteRow = new TextButton("X", Main.resource.uiSkin);
        final TextButton atlasButton = new TextButton(isAtlas.get() ? ATLAS_ANIMATION : IMAGE_ANIMATION, Main.resource.uiSkin);
        final TextField tileWidthField = new TextField("1", Main.resource.uiSkin);
        final TextField tileHeightField = new TextField("1", Main.resource.uiSkin);
        final TextField countTextureField = new TextField("1", Main.resource.uiSkin);
        final TextField frameDurationField = new TextField("0.5", Main.resource.uiSkin);
        final TextField widthField = new TextField("", Main.resource.uiSkin);
        final TextField heightField = new TextField("", Main.resource.uiSkin);
        final TextField xField = new TextField("", Main.resource.uiSkin);
        final TextField yField = new TextField("", Main.resource.uiSkin);
        final TextButton addGroupButton = new TextButton("Add group", Main.resource.uiSkin);
        AnimationLayerModel animationLayerModel = new AnimationLayerModel();
        List<Integer> groupList = new ArrayList<>();
        Map<Integer, Integer> groupValueMap = new HashMap<>();
        animationLayerModel.setGroup(groupList);
        AnimationData animationData = new AnimationData(animationLayerModel, "", 0, 1, 1);
        animationDataArray.add(animationData);

        Table table = addAnimationRow(inputField, pathButton, deleteRow, atlasButton, tileWidthField, tileHeightField, countTextureField, frameDurationField, widthField, heightField, xField, yField, addGroupButton, groupTable);

        inputField.setTextFieldListener((textField, c) ->
            setAnimationLayer(animationData,
                    isAtlas.get(),
                    inputField.getText(),
                    Integer.parseInt(tileWidthField.getText()),
                    Integer.parseInt(tileHeightField.getText()),
                    Integer.parseInt(countTextureField.getText()),
                    Float.parseFloat(frameDurationField.getText()),
                    getFloatFromField(widthField),
                    getFloatFromField(heightField),
                    getFloatFromField(xField),
                    getFloatFromField(yField)
            )
        );

        pathButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MainScreen.callFileChooser(new FileChooserAdapter() {
                    @Override
                    public void selected(Array<FileHandle> files) {
                        inputField.setText(files.first().path());
                        setAnimationLayer(animationData,
                                isAtlas.get(),
                                inputField.getText(),
                                Integer.parseInt(tileWidthField.getText()),
                                Integer.parseInt(tileHeightField.getText()),
                                Integer.parseInt(countTextureField.getText()),
                                Float.parseFloat(frameDurationField.getText()),
                                getFloatFromField(widthField),
                                getFloatFromField(heightField),
                                getFloatFromField(xField),
                                getFloatFromField(yField)
                        );
                    }
                });
            }
        });

        deleteRow.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                table.clear();
                animationDataArray.removeValue(animationData, true);

                Array<Cell> cellArray = scrollAnimationTable.getCells();
                for (Cell cell : cellArray) {
                    if (cell.getActor() == table) {
                        cell.clearActor().pad(0);
                    }
                }
                cellArray.sort((Cell c1, Cell c2) -> {
                    if (c1.getActor() == null && c2.getActor() != null) {
                        return 1;
                    }

                    if (c1.getActor() != null && c2.getActor() == null) {
                        return -1;
                    }

                    return 0;
                });
                drawAnimationLayer();
            }
        });

        atlasButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                isAtlas.set(!isAtlas.get());
                atlasButton.setText(isAtlas.get() ? ATLAS_ANIMATION : IMAGE_ANIMATION);
                if (isAtlas.get()) {
                    tileWidthField.setVisible(true);
                    tileHeightField.setVisible(true);
                } else {
                    tileWidthField.setVisible(false);
                    tileHeightField.setVisible(false);
                }
                setAnimationLayer(animationData,
                        isAtlas.get(),
                        inputField.getText(),
                        Integer.parseInt(tileWidthField.getText()),
                        Integer.parseInt(tileHeightField.getText()),
                        Integer.parseInt(countTextureField.getText()),
                        Float.parseFloat(frameDurationField.getText()),
                        getFloatFromField(widthField),
                        getFloatFromField(heightField),
                        getFloatFromField(xField),
                        getFloatFromField(yField)
                );
            }
        });

        TextField.TextFieldListener textFieldListener = (textField, c) -> {
            try {
                new Integer(textField.getText());
            } catch (NumberFormatException e) {
                textField.setText("1");
            }
            setAnimationLayer(animationData,
                    isAtlas.get(),
                    inputField.getText(),
                    Integer.parseInt(tileWidthField.getText()),
                    Integer.parseInt(tileHeightField.getText()),
                    Integer.parseInt(countTextureField.getText()),
                    Float.parseFloat(frameDurationField.getText()),
                    getFloatFromField(widthField),
                    getFloatFromField(heightField),
                    getFloatFromField(xField),
                    getFloatFromField(yField)
            );
        };

        countTextureField.setTextFieldListener(textFieldListener);
        tileWidthField.setTextFieldListener(textFieldListener);
        tileHeightField.setTextFieldListener(textFieldListener);

        frameDurationField.setTextFieldListener(((textField, c) -> {
            textField.setText(textField.getText().replace(",", "."));
            try {
                new Float(textField.getText());
            } catch (NumberFormatException e) {
                textField.setText("0.5");
            }
            setAnimationLayer(animationData,
                    isAtlas.get(),
                    inputField.getText(),
                    Integer.parseInt(tileWidthField.getText()),
                    Integer.parseInt(tileHeightField.getText()),
                    Integer.parseInt(countTextureField.getText()),
                    Float.parseFloat(frameDurationField.getText()),
                    getFloatFromField(widthField),
                    getFloatFromField(heightField),
                    getFloatFromField(xField),
                    getFloatFromField(yField));
        }));

        widthField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setAnimationLayer(animationData,
                        isAtlas.get(),
                        inputField.getText(),
                        Integer.parseInt(tileWidthField.getText()),
                        Integer.parseInt(tileHeightField.getText()),
                        Integer.parseInt(countTextureField.getText()),
                        Float.parseFloat(frameDurationField.getText()),
                        getFloatFromField(widthField),
                        getFloatFromField(heightField),
                        getFloatFromField(xField),
                        getFloatFromField(yField)
                );
            }
        });

        heightField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setAnimationLayer(animationData,
                        isAtlas.get(),
                        inputField.getText(),
                        Integer.parseInt(tileWidthField.getText()),
                        Integer.parseInt(tileHeightField.getText()),
                        Integer.parseInt(countTextureField.getText()),
                        Float.parseFloat(frameDurationField.getText()),
                        getFloatFromField(widthField),
                        getFloatFromField(heightField),
                        getFloatFromField(xField),
                        getFloatFromField(yField)
                );
            }
        });

        xField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setAnimationLayer(animationData,
                        isAtlas.get(),
                        inputField.getText(),
                        Integer.parseInt(tileWidthField.getText()),
                        Integer.parseInt(tileHeightField.getText()),
                        Integer.parseInt(countTextureField.getText()),
                        Float.parseFloat(frameDurationField.getText()),
                        getFloatFromField(widthField),
                        getFloatFromField(heightField),
                        getFloatFromField(xField),
                        getFloatFromField(yField)
                );
            }
        });

        yField.setTextFieldListener(new FloatFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                super.keyTyped(textField, c);
                setAnimationLayer(animationData,
                        isAtlas.get(),
                        inputField.getText(),
                        Integer.parseInt(tileWidthField.getText()),
                        Integer.parseInt(tileHeightField.getText()),
                        Integer.parseInt(countTextureField.getText()),
                        Float.parseFloat(frameDurationField.getText()),
                        getFloatFromField(widthField),
                        getFloatFromField(heightField),
                        getFloatFromField(xField),
                        getFloatFromField(yField)
                );
            }
        });

        addGroupButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                addGroupCell(groupTable, groupValueMap, groupList);
            }
        });
    }

    private Table addAnimationRow(
            final TextField inputField,
            final TextButton pathButton,
            final TextButton deleteRow,
            final TextButton atlasButton,
            final TextField tileWidthField,
            final TextField tileHeightField,
            final TextField countTextureField,
            final TextField frameDurationField,
            final TextField widthField,
            final TextField heightField,
            final TextField xField,
            final TextField yField,
            final TextButton addGroupButton,
            final Table groupTable
    ) {
        Table table = null;
        Array<Cell> cellArray = scrollAnimationTable.getCells();
        for (Cell cell : cellArray) {
            if (cell.getActor() == null) {
                table = new Table();
                cell.setActor(table).pad(1);
                break;
            }
        }

        if (table == null) {
            table = new Table();
            scrollAnimationTable.add(table).pad(1);
        }
        table.setBackground(Main.resource.uiSkin.getDrawable("border"));

        float rowWidth = (mainWindow.getWidth() / 10) * 0.92f;

        table.add(inputField)
                .colspan(2)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(atlasButton)
                .colspan(2)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(pathButton)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padRight(SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(deleteRow)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padRight(SPACE_SCROLL_ELEMENT * 2)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.row();


        table.add(tileWidthField)
                .colspan(2)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(tileHeightField)
                .colspan(2)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(countTextureField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padRight(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(frameDurationField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padRight(SPACE_SCROLL_ELEMENT * 2)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.row();

        table.add(widthField)
                .colspan(2)
                .width((rowWidth * 3)- SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(heightField)
                .colspan(2)
                .width((rowWidth * 3) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(xField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2)
                .padRight(SPACE_SCROLL_ELEMENT);
        table.add(yField)
                .width((rowWidth * 2) - (SPACE_SCROLL_ELEMENT * 2))
                .padTop(SPACE_SCROLL_ELEMENT /2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2)
                .padRight(SPACE_SCROLL_ELEMENT * 2);
        table.row();

        table.add(addGroupButton)
                .colspan(4)
                .width((rowWidth * 6) - SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);
        table.add(groupTable).width((rowWidth * 4) - (SPACE_SCROLL_ELEMENT * 2))
                .colspan(2)
                .padRight(SPACE_SCROLL_ELEMENT)
                .padLeft(SPACE_SCROLL_ELEMENT)
                .padTop(SPACE_SCROLL_ELEMENT / 2)
                .padBottom(SPACE_SCROLL_ELEMENT / 2);

        scrollAnimationTable.row();
        return table;
    }

    private void setImageLayer(ImageLayerModel imageLayer, Float width, Float height, Float x, Float y) {
        imageLayer.setWidth(width);
        imageLayer.setHeight(height);
        imageLayer.setX(x);
        imageLayer.setY(y);

        drawImageLayer();
    }

    private void setAnimationLayer(AnimationData animationData, boolean isAtlas, String path, int tileWidth, int tileHeight, int countFrame, float frameDuration, Float width, Float height, Float x, Float y) {
        AnimationLayerModel animationLayerModel = animationData.getAnimationLayerModel();

        if (!animationData.getPath().equals(path) || animationData.getCountFrame() != countFrame || animationData.getTileWidth() != tileWidth || animationData.getTileHeight() != tileHeight) {
            try {
                if (isAtlas) {
                    animationLayerModel.setAnimationFrame(new Texture(Gdx.files.internal(path), true), tileWidth, tileHeight, countFrame, frameDuration, width, height, x, y);
                } else {
                    animationLayerModel.setAnimationFrame(path, countFrame, frameDuration, width, height, x, y, true);
                }
            } catch (Exception e) {
                animationLayerModel.setAnimationFrame(null, 0, width, height, x, y, true);
            }
        } else {
            animationLayerModel.setFrameDuration(frameDuration);
            animationLayerModel.setWidth(width);
            animationLayerModel.setHeight(height);
            animationLayerModel.setX(x);
            animationLayerModel.setY(y);
        }

        animationData.setCountFrame(countFrame);
        animationData.setPath(path);
        animationData.setTileWidth(tileWidth);
        animationData.setTileHeight(tileHeight);

        drawAnimationLayer();
    }

    private Float getFloatFromField(TextField textField) {
        try {
            return new Float(textField.getText());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private class FloatFieldListener implements TextField.TextFieldListener {

        @Override
        public void keyTyped(TextField textField, char c) {
            textField.setText(textField.getText().replace(",", "."));
            try {
                new Float(textField.getText());
            } catch (NumberFormatException e) {
                textField.setText("");
            }
        }
    }
}
